#!/usr/bin/env sh
USB_BASEPATH="/dev/bus/usb"

function _usage {
    echo "Usage: helpers.sh VENDOR_ID:[DEVICE_ID]"
    echo ""
    echo "Find a USB device path given a vendor ID and optionally a device ID."
    echo "IDs can be read from e.g. dmesg output after plugging the device in."
    echo ""
    echo "Exit codes:"
    echo "  0: Success"
    echo "  1: Failure"
}

function _err {
    echo "[ERR ] $@" 1>&2
}

function _info {
    echo "[INFO] $@" 1>&2
}

# Given a USB device vendor ID ($1), print out the path under /dev/bus/usb that
# belongs to it.
function _get_usb_dev_paths {
    VENDOR_ID="$1"
    CANDIDATES=$(lsusb -d $VENDOR_ID)
    if [[ "a$CANDIDATES" == "a" ]]; then
        _err "Found no devices with Vendor ID $VENDOR_ID"
        exit 1
    fi

    NUMDEV=$(wc -l <<<"$CANDIDATES")
    if [[ $NUMDEV -ne 1 ]]; then
        _err "Expected exactly one USB device with Vendor ID $VENDOR_ID, found $NUMDEV"
        exit 1
    fi

    BUSID=$(echo $CANDIDATES | cut -f2 -d' ')
    DEVID=$(echo $CANDIDATES | cut -f4 -d' ' | tr -d ':')
    USBPATH="$USB_BASEPATH/$BUSID/$DEVID"
    if ! [[ -c $USBPATH ]]; then
        _err "USB device matching $VENDOR_ID cannot be found under /dev"
        exit 1
    fi
    _info "Found USB device matching $VENDOR_ID at $USBPATH"

    echo $USBPATH
}

# Main entry point
if [[ $# -lt 1 ]]; then
    _usage
    exit 1
fi

_get_usb_dev_paths "$1"

